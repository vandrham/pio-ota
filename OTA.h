#ifndef ota_h
#define ota_h

#include "WiFiBase.h"

class OTA
{
    public:
        OTA(WiFiBase *wifi);
        void setup();
        void loop();
    private:
        WiFiBase *_wifi;
};

#endif