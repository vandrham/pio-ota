# pio-ota

platformio.ini:

  * lib_deps = git@bitbucket.org:vandrham/pio-ota.git

```
#include <OTA.h>

WiFiBase wifi("ssid", "password");
OTA ota(&wifi);

void setup()
{
	ota.setup();
}

void loop()
{
	ota.loop();
}

```