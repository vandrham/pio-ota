#include "OTA.h"
#include <WiFiBase.h>
#include <Arduino.h>
#include <ArduinoOTA.h>

OTA::OTA(WiFiBase *wifi)
{
    _wifi = wifi;
}

void OTA::setup()
{
   _wifi -> connect();

    ArduinoOTA.onStart([]()
    {
#if HUZZAH
        digitalWrite(LED_BUILTIN, LOW);
        digitalWrite(2, LOW);
#endif
    });

    ArduinoOTA.onProgress([](unsigned int progress, unsigned int total)
    {
#if HUZZAH
        digitalWrite(LED_BUILTIN, HIGH);
#endif
    });

    ArduinoOTA.onEnd([]()
    {
#if HUZZAH
        digitalWrite(2, HIGH);
#endif
    });

    ArduinoOTA.onError([](ota_error_t error)
    {
#if HUZZAH
        digitalWrite(LED_BUILTIN, LOW);
        digitalWrite(2, LOW);
        delay(200);
        digitalWrite(LED_BUILTIN, HIGH);
        digitalWrite(2, HIGH);
        delay(200);
        digitalWrite(LED_BUILTIN, LOW);
        digitalWrite(2, LOW);
        delay(200);
        digitalWrite(LED_BUILTIN, HIGH);
        digitalWrite(2, HIGH);
        delay(200);
        digitalWrite(LED_BUILTIN, LOW);
        digitalWrite(2, LOW);
#endif
        ESP.restart();
    });
    ArduinoOTA.begin();
}

void OTA::loop()
{
    _wifi -> connect(5);
    ArduinoOTA.handle();
}